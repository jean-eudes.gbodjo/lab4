import sys
import time
import os
import tensorflow as tf
import numpy as np
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score
from sklearn.preprocessing import LabelEncoder
from sklearn.utils import shuffle

def format_to_cnn1d (X, n_bands):
    new_X = np.reshape(X,(X.shape[0],n_timestamps,-1))
    print (new_X.shape)
    return new_X

def format_label (y, n_classes, onehot=True) :
    encoder = LabelEncoder()
    y_tr = encoder.fit_transform(y)
    if onehot :
        y_tr = tf.keras.utils.to_categorical(y_tr, n_classes,dtype='float32')
    print (y_tr.shape)
    return y_tr


def get_batch(array, i, batch_size):
    start_id = i*batch_size
    end_id = min((i+1) * batch_size, array.shape[0])
    batch = array[start_id:end_id]
    return batch

def conv1d (X, n_filters, k_size, name, padding_mode="valid", strides=False, activate=True, bias=True) :
    if strides :
        conv = tf.keras.layers.Conv1D(filters=n_filters, kernel_size=k_size, strides= 2, padding=padding_mode, use_bias=bias, name=name)(X)
    else :
        conv = tf.keras.layers.Conv1D(filters=n_filters, kernel_size=k_size, padding=padding_mode, use_bias=bias, name=name)(X)
    conv = tf.keras.layers.BatchNormalization(name="%s_batchnorm"%name)(conv)
    if activate :
        conv = tf.nn.relu(conv,name="%s_relu"%name)
    return conv

def add_fc(feat,n_units,n_classes,dropOut,name):
    with tf.variable_scope("fc"):
        fc1 = tf.keras.layers.Dense(n_units,name="fc1_%s"%name)(feat)
        fc1 = tf.keras.layers.BatchNormalization(name="batchnorm_%s"%name)(fc1)
        fc1 = tf.nn.relu(fc1)
        fc1 = tf.nn.dropout(fc1, rate=dropOut)
        # tf.summary.histogram("fc1_activation",fc1)

        fc2 = tf.keras.layers.Dense(n_units/2,name="fc2_%s"%name)(fc1)
        fc2 = tf.keras.layers.BatchNormalization(name="batchnorm_%s"%name)(fc2)
        fc2 = tf.nn.relu(fc2)
        # fc2 = tf.nn.dropout(fc2, rate=dropOut)
        # tf.summary.histogram("fc2_activation",fc1)

        logits = tf.keras.layers.Dense(n_classes,name="nclasses_%s"%name)(fc2)
    return logits

def cnn1d(X, n_filters, dropOut):
    with tf.variable_scope("cnn1d"):
        conv1 = conv1d(X, n_filters, 5, name="conv1",)
        conv1 = tf.nn.dropout(conv1, rate=dropOut)
        # tf.summary.histogram("conv1_drop",conv1)

        conv2 = conv1d(conv1 ,n_filters, 3, name="conv2", strides=True)
        conv2 = tf.nn.dropout(conv2, rate=dropOut)
        # tf.summary.histogram("conv2_drop",conv2)

        conv3 = conv1d(conv2 ,n_filters*2, 3, name="conv3")
        conv3 = tf.nn.dropout(conv3, rate=dropOut)
        # tf.summary.histogram("conv3_drop",conv3)

        conv4 = conv1d(conv3 ,n_filters*2, 3, name="conv4")
        conv4 = tf.nn.dropout(conv4, rate=dropOut)
        # tf.summary.histogram("conv4_drop",conv4)

        conv5 = conv1d(conv4 ,n_filters*2, 1, name="conv5")
        # tf.summary.histogram("conv5_drop",conv5)

        flatten = tf.layers.flatten(conv5)
        flatten = tf.nn.dropout(flatten, rate=dropOut)
        # tf.summary.histogram("flatten",flatten)

    return conv5, flatten

def run(train_X, train_y, valid_X, valid_y, valid_label, output_dir_models, split_numb, n_timestamps,
            n_classes, n_filters, fc_units, batch_size, n_epochs, learning_rate, drop, level):
    n_bands = train_X.shape[2]

    X = tf.placeholder(tf.float32,shape=(None,n_timestamps,n_bands),name='X')
    y = tf.placeholder(tf.float32,shape=(None,n_classes),name='y')
    dropOut = tf.placeholder(tf.float32, shape=(), name="drop_rate")
    
    _, feat = cnn1d(X, n_filters, dropOut)

    logits = add_fc(feat,fc_units,n_classes,dropOut,name="fc_feat")

    with tf.variable_scope("prediction"):

        prediction = tf.math.argmax(logits,1,name="pred")
        # tf.summary.histogram("prediction",prediction)
        
        correct = tf.math.equal(tf.math.argmax(logits,1),tf.argmax(y,1))
        accuracy = tf.reduce_mean(tf.dtypes.cast(correct,tf.float64))
        # tf.summary.scalar("accuracy",accuracy)

    with tf.variable_scope("cost"):
        cost = tf.math.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(labels=y,logits=logits))

        # tf.summary.scalar("cost",cost)

    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)
    
    # merged = tf.summary.merge_all()

    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

    n_batch = int(train_X.shape[0] / batch_size)
    if train_X.shape[0] % batch_size != 0:
        n_batch+=1
    print ("n_batch: %d" %n_batch)

    saver = tf.train.Saver()
    best_acc = sys.float_info.min

    init = tf.global_variables_initializer()
    with tf.Session() as session:
        session.run(init)

        # For Tensorboard visualization
        # log_dir = os.path.join(output_dir_models,"logdir",str(split_numb),"run_%s"%time.strftime("%m%d_%H%M"))
        # if not os.path.isdir(log_dir):
        #     os.makedirs(log_dir)
        # train_writer = tf.summary.FileWriter(log_dir+"/train")
        # train_writer.add_graph(session.graph)
        # valid_writer = tf.summary.FileWriter(log_dir+"/valid")

        for epoch in range(1,n_epochs+1):
            start = time.time()
            epoch_loss = 0
            epoch_acc = 0
            train_X, train_y = shuffle(train_X, train_y,random_state=8896)

            for batch in range(n_batch):
                batch_X = get_batch(train_X,batch,batch_size)
                batch_y = get_batch(train_y,batch,batch_size)

                acc, loss,_ = session.run([accuracy, cost, optimizer],feed_dict={X:batch_X,
                                                                                 y:batch_y,
                                                                                 dropOut:drop})
                
                # if batch % 50 == 0 :
                #     summary, _ = session.run([merged, optimizer],feed_dict={X:batch_X,
                #                                                                 y:batch_y,
                #                                                                 dropOut:drop})
                #     train_writer.add_summary(summary,batch)

                epoch_loss += loss
                epoch_acc += acc

                del batch_X
                del batch_y

            stop = time.time()
            elapsed = stop - start
            print ("Epoch ",epoch, " Train loss:",epoch_loss/n_batch,"| Accuracy:",epoch_acc/n_batch, "| Time: ",elapsed)

            # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

            valid_batch = int(valid_X.shape[0] / (4*batch_size))
            if valid_X.shape[0] % (4*batch_size) != 0:
                valid_batch+=1

            total_pred = None

            for ibatch in range(valid_batch):
                valid_batch_X = get_batch(valid_X,ibatch,4*batch_size)
                # valid_batch_y = get_batch(valid_y,ibatch,4*batch_size)

                batch_pred = session.run(prediction,feed_dict={X:valid_batch_X,dropOut:0})
                
                # valid_summary, batch_pred = session.run([merged,prediction],feed_dict={X:valid_batch_X,
                #                                                                        y:valid_batch_y,
                #                                                                        dropOut:0})
                # valid_writer.add_summary(valid_summary,ibatch)

                del valid_batch_X
                # del valid_batch_y

                if total_pred is None :
                    total_pred = batch_pred
                else :
                    total_pred = np.hstack((total_pred,batch_pred))

            print ("PREDICTION")
            print ("LEVEL %d"%level)
            print (np.bincount(np.array(total_pred)))
            print (np.bincount(np.array(valid_label)))
            print ("TEST F-Measure: %f" %f1_score(valid_label, total_pred, average='weighted'))
            print (f1_score(valid_label, total_pred, average=None))
            val_acc = accuracy_score(valid_label, total_pred)
            print ("TEST Accuracy: %f" % val_acc)

            
            if val_acc > best_acc :
                save_path = saver.save(session, output_dir_models+"/model_"+str(split_numb))
                print("Model saved in path: %s" % save_path)
                best_acc = val_acc

if __name__ == '__main__':
    print (tf.__version__)

    # Reading data
    train_ts = np.load(sys.argv[1])
    # print ("train_ts:", train_ts.shape)

    train_label = np.load(sys.argv[2])
    # print ("train_label:", train_label.shape)

    valid_ts = np.load(sys.argv[3])
    # print ("valid_ts:", valid_ts.shape)

    valid_label = np.load(sys.argv[4])
    # print ("valid_label:", valid_label.shape)

    split_numb = int(sys.argv[5])
    output_dir_models = sys.argv[6]
    n_timestamps = int(sys.argv[7])

    level = int(sys.argv[8])

    sys.stdout.flush

    train_label = train_label[:,level]
    train_label = train_label.astype('int64')
    valid_label = valid_label[:,level]
    valid_label = valid_label.astype('int64')

    n_classes = len(np.unique(train_label))

    # Format data and label
    train_X = format_to_cnn1d(train_ts, n_timestamps)
    train_y = format_label(train_label,n_classes)
    valid_X = format_to_cnn1d(valid_ts, n_timestamps)
    valid_label = format_label(valid_label,n_classes,onehot=False)
    valid_y = format_label(valid_label,n_classes)

    # Run Model
    site = output_dir_models.split("/")[1]

    if site == "reunion":
        n_filters = 128
        fc_units = n_filters*4

        batch_size = 32
        n_epochs = 1000
        learning_rate = 1E-4
        drop = 0.5 

    elif site == "senegal":
        n_filters = 128
        fc_units = n_filters*2

        batch_size = 32
        n_epochs = 1000
        learning_rate = 1E-4
        drop = 0.5
    
    elif site == "dordogne":
        n_filters = 32 #64
        fc_units = n_filters*8

        batch_size = 32
        n_epochs = 1000
        learning_rate = 1E-4
        drop = 0.5 

    run(train_X, train_y, valid_X, valid_y, valid_label, output_dir_models, split_numb, n_timestamps,
             n_classes, n_filters, fc_units, batch_size, n_epochs, learning_rate, drop, level-1)
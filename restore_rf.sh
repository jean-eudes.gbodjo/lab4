#!/bin/bash

site="$1/optTimeSeries"
pt=50

for level in {0..2}
do
    for i in {0..9}
    do
        echo $i
        python restore_rf.py rf/$1/l$level splits/$site $i $pt
    done
done
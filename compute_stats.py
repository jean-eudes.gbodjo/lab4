import numpy as np
import sys
import glob
import os
from sklearn.metrics import cohen_kappa_score
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score


def transform(val):
	return np.round((float( int(val*10000) ) / 100),2)
	

gt_folder = sys.argv[1] 
result_folder = sys.argv[2] 
train_perc = sys.argv[3] 
level = int(sys.argv[4]) 

fm = []
kappa = []
acc = []

vec = None
count = 0
for i in range(10): 
	gtFName = gt_folder+"/test_y"+str(i)+"_"+str(train_perc)+".npy"

	clFName = result_folder+"/results_"+str(i)+".npy"

	if os.path.exists(clFName):
		# print ("LOAD")
		gt = np.load(gtFName)
		gt = gt[:,level] 
		cl = np.load(clFName)
		if len(cl.shape) > 1:
			cl = cl[:,level-1]

		kappa.append( cohen_kappa_score(gt,cl) )
		fm.append( f1_score(gt,cl, average="weighted") )
		acc.append( accuracy_score(gt,cl))
		print ("%d: %f" % (i, f1_score(gt,cl, average="weighted") ))
		if vec is None:	
			vec = np.array( f1_score(gt,cl, average=None) )
		else:
			vec = np.vstack((vec,np.array( f1_score(gt,cl, average=None) )))
		count+=1
		print (np.array( f1_score(gt,cl, average=None) ))

print ("%.2f $\\pm$ %.2f & %.2f $\\pm$ %.2f & %.2f $\\pm$ %.2f" %(transform(np.mean(fm)), transform( np.std(fm)), round(np.mean(kappa),2) , round(np.std(kappa),2), transform( np.mean(acc) ), transform( np.std(acc) )) +" \\" + "\\" + " \\hline")
nvec = np.mean(vec,axis=0)
print (' & '.join("%.2f"%transform(el) for el in nvec) + " \\" + "\\" + " \\hline")



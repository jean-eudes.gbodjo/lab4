import sys
import time
import os
import tensorflow as tf
print (tf.__version__)
import numpy as np
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score
from sklearn.preprocessing import LabelEncoder
from sklearn.utils import shuffle
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score
from cnn1d import add_fc, conv1d, cnn1d

def format_to_cnn1d (X, n_bands):
    new_X = np.reshape(X,(X.shape[0],n_timestamps,-1))
    print (new_X.shape)
    return new_X

def format_label (y, n_classes, onehot=True) :
    y_tr = None
    for c in range(len(n_classes)):
        encoder = LabelEncoder()
        encoded = encoder.fit_transform(y[:,c+1])
        if onehot :
            encoded = tf.keras.utils.to_categorical(encoded, n_classes[c],dtype='float32')
            if y_tr is None :
                y_tr = encoded
            else :
                y_tr = np.hstack((y_tr,encoded))
        else :
            if y_tr is None :
                y_tr = np.reshape(encoded,(encoded.shape[0],1))
            else :
                y_tr = np.hstack((y_tr,np.reshape(encoded,(encoded.shape[0],1))))
    print (y_tr.shape)
    return y_tr

def get_batch(array, i, batch_size):
    start_id = i*batch_size
    end_id = min((i+1) * batch_size, array.shape[0])
    batch = array[start_id:end_id]
    return batch

def rnn(feat, rnn_units, fc_units, n_classes, is_training, dropOut) :
    seq = [feat]*len(n_classes)
    with tf.variable_scope("rnn"):
        cell = tf.nn.rnn_cell.GRUCell(rnn_units)
        cell = tf.nn.rnn_cell.DropoutWrapper(cell,output_keep_prob=1-dropOut, state_keep_prob=1-dropOut)
        outputs, _ = tf.nn.static_rnn(cell, seq, dtype=tf.float32)

        logits = []
        for l in range(len(outputs)) :
            logits.append(add_fc(outputs[l],fc_units,n_classes[l],dropOut,is_training,name="feat_l%s"%l)) #len(outputs)-1-l
    return logits

def run(train_X, train_y, valid_X, valid_y, output_dir_models, split_numb, n_timestamps, 
            n_classes, n_filters, rnn_units, fc_units, batch_size, n_epochs, learning_rate, drop):
    n_bands = train_X.shape[2]
    
    X = tf.placeholder(tf.float32,shape=(None,n_timestamps,n_bands),name='X')
    y = tf.placeholder(tf.float32,shape=(None,np.sum(n_classes)),name='y')
    yl = tf.split(y,n_classes,axis=1)
    dropOut = tf.placeholder(tf.float32, shape=(), name="drop_rate")
    is_training = tf.placeholder(tf.bool, shape=(), name="is_training")

    feat = cnn1d(X, n_filters, is_training, dropOut)
    logits_cnn1d = tf.keras.layers.Dense(n_classes[-1],name="feat_cnn1d")(feat)
    logits = rnn(feat, rnn_units, fc_units, n_classes, is_training, dropOut)

    with tf.variable_scope("prediction"):
        lst_accuracy = []
        lst_prediction = []
        for l in range(len(logits)) :
            lst_prediction.append(tf.math.argmax(logits[l],1))
            correct = tf.math.equal(tf.math.argmax(logits[l],1),tf.argmax(yl[l],1)) #len(logits)-1-l
            lst_accuracy.append(tf.reduce_mean(tf.dtypes.cast(correct,tf.float64)))
        prediction = tf.stack(lst_prediction,axis=1,name="all_pred")
    
        tf.summary.histogram("levels_accuracy",lst_accuracy)

    with tf.variable_scope("cost"):
        cost = tf.math.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(labels=yl[-1],logits=logits_cnn1d))
        weight = [1/3,2/3,1]
        for l in range(len(logits)) : 
            cost += weight[l]*tf.math.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(labels=yl[l],logits=logits[l])) #len(logits)-1-l
    
        tf.summary.scalar("cost",cost)

    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)

    merged = tf.summary.merge_all()

    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

    n_batch = int(train_X.shape[0] / batch_size)
    if train_X.shape[0] % batch_size != 0:
        n_batch+=1
    print ("n_batch: %d" %n_batch)

    saver = tf.train.Saver()
    best_acc = np.array([sys.float_info.min]*len(n_classes))

    init = tf.global_variables_initializer()
    with tf.Session() as session:
        session.run(init)

        # For Tensorboard visualization
        log_dir = os.path.join(output_dir_models,"logdir",str(split_numb),"run_%s"%time.strftime("%m%d_%H%M"))
        if not os.path.isdir(log_dir):
            os.makedirs(log_dir)
        train_writer = tf.summary.FileWriter(log_dir+"/train")
        train_writer.add_graph(session.graph)

        for epoch in range(1,n_epochs+1):
            start = time.time()
            epoch_loss = 0
            epoch_acc = []
            train_X, train_y = shuffle(train_X, train_y,random_state=8896)

            for batch in range(n_batch):
                batch_X = get_batch(train_X,batch,batch_size)
                batch_y = get_batch(train_y,batch,batch_size)

                acc, loss,_ = session.run([lst_accuracy, cost, optimizer],feed_dict={X:batch_X,
                                                                                     y:batch_y,
                                                                                     dropOut:drop,
                                                                                     is_training: True})
                if batch % 50 == 0 :
                    summary, _ = session.run([merged, optimizer],feed_dict={X:batch_X,
                                                                            y:batch_y,
                                                                            dropOut:drop,
                                                                            is_training: True})
                    train_writer.add_summary(summary,batch)
                
                epoch_loss += loss
                epoch_acc.append(acc)

                del batch_X
                del batch_y
 
            stop = time.time()
            elapsed = stop - start
            mean_epoch_acc = np.mean(np.stack(epoch_acc,axis=0),axis=0)
            print ("Epoch ",epoch, " Train loss:",epoch_loss/n_batch,"| Accuracy:",mean_epoch_acc, "| Time: ",elapsed)

            # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

            valid_batch = int(valid_X.shape[0] / (4*batch_size))
            if valid_X.shape[0] % (4*batch_size) != 0:
                valid_batch+=1

            #valid_writer = tf.summary.FileWriter(log_dir+"/valid")

            total_pred = None
            for ibatch in range(valid_batch):
                valid_batch_X = get_batch(valid_X,ibatch,4*batch_size)

                batch_pred = session.run(prediction,feed_dict={X:valid_batch_X,
                                                               dropOut:0,
                                                               is_training: False})
                # if batch % 50 == 0 :
                #     summary, _ = session.run([merged, optimizer],feed_dict={X:valid_batch_X,
                #                                                             y:batch_y,
                #                                                             dropOut:0
                #                                                             is_training: False})
                #     valid_writer.add_summary(summary,batch)

                del valid_batch_X
                
                if total_pred is None :
                    total_pred = batch_pred
                else : 
                    total_pred = np.vstack((total_pred,batch_pred))

            
            print ("PREDICTION")
            val_acc = []
            for c in range(len(n_classes)):
                print ("LEVEL %d"%c) #len(n_classes)-1-c)
                print (np.bincount(np.array(total_pred[:,c])))
                print (np.bincount(np.array(valid_y[:,c]))) #len(n_classes)-1-c
                print ("TEST F-Measure: %f" %f1_score(valid_y[:,c], total_pred[:,c], average='weighted')) #len(n_classes)-1-c
                print (f1_score(valid_y[:,c], total_pred[:,c], average=None)) #len(n_classes)-1-c
                print ("TEST Accuracy: %f" % accuracy_score(valid_y[:,c], total_pred[:,c])) #len(n_classes)-1-c

                val_acc.append(accuracy_score(valid_y[:,c], total_pred[:,c])) #len(n_classes)-1-c
            val_acc = np.array(val_acc) 

            if np.count_nonzero(np.greater(val_acc, best_acc)) == val_acc.size :
                save_path = saver.save(session, output_dir_models+"/model_"+str(split_numb))
                print("Model saved in path: %s" % save_path)
                best_acc = val_acc
   
if __name__ == '__main__':

    # Reading data
    train_ts = np.load(sys.argv[1])
    # print ("train_ts:", train_ts.shape)

    train_label = np.load(sys.argv[2])
    train_label = train_label.astype('int64')
    # print ("train_label:", train_label.shape)

    n_classes = ()
    for l in range(1,train_label.shape[1]) :
        n_classes += (len(np.unique(train_label[:,l])),)

    valid_ts = np.load(sys.argv[3])
    # print ("valid_ts:", valid_ts.shape)

    valid_label = np.load(sys.argv[4])
    valid_label = valid_label.astype('int64')
    # print ("valid_label:", valid_label.shape)

    split_numb = int(sys.argv[5])
    output_dir_models = sys.argv[6]
    n_timestamps = int(sys.argv[7])

    sys.stdout.flush

    # Format data and label
    train_X = format_to_cnn1d(train_ts, n_timestamps)
    train_y = format_label(train_label,n_classes)
    valid_X = format_to_cnn1d(valid_ts, n_timestamps)
    valid_y = format_label(valid_label,n_classes,onehot=False)

    # Run Model
    n_filters = 64
    fc_units = n_filters*8
    rnn_units = 512

    batch_size = 32
    n_epochs = 3000
    learning_rate = 1E-4
    drop = 0.3

    run(train_X, train_y, valid_X, valid_y, output_dir_models, split_numb, n_timestamps, 
             n_classes, n_filters, rnn_units, fc_units, batch_size, n_epochs, learning_rate, drop)
import sys
import glob, os
from pprint import pprint
import rasterio
import numpy as np
from sklearn.preprocessing import MinMaxScaler
import gdal, ogr
import geopandas as gpd
import random

def normalize_time_series (seg_ts_file ,gt_ts_file, n_bands) :
    
    outPath = os.path.dirname(seg_ts_file)

    # if not os.path.isfile(os.path.join(outPath,"gapf_ts_data.npy")) : 
    seg_ts = np.load(seg_ts_file)
    n_timestamps = int(seg_ts.shape[1]/n_bands)
    # print (n_timestamps)

    gt_ts = np.load(gt_ts_file)

    norm_seg_ts = None
    norm_gt_ts = None
    
    for i in range(n_bands):
        # print (i*n_timestamps,(i+1)*n_timestamps)
        band_min = np.min(seg_ts[:,i*n_timestamps:(i+1)*n_timestamps])
        band_max = np.max(seg_ts[:,i*n_timestamps:(i+1)*n_timestamps])
        print (band_min, band_max)

        if norm_seg_ts is None :
            norm_seg_ts = (seg_ts[:,i*n_timestamps:(i+1)*n_timestamps] - band_min) / (band_max - band_min)
        else :
            norm_seg_ts = np.column_stack((norm_seg_ts,((seg_ts[:,i*n_timestamps:(i+1)*n_timestamps] - band_min) / (band_max - band_min))))

        if norm_gt_ts is None :
            norm_gt_ts = (gt_ts[:,i*n_timestamps:(i+1)*n_timestamps] - band_min) / (band_max - band_min)
        else :
            norm_gt_ts = np.column_stack((norm_gt_ts,((gt_ts[:,i*n_timestamps:(i+1)*n_timestamps] - band_min) / (band_max - band_min))))

    print (norm_seg_ts.shape)
    print (norm_gt_ts.shape)

    seg_ts_array = None
    for j in range(n_timestamps): # Time stamps
        lst = []
        for c in range(n_bands) : # bands + indices number
            lst.append(norm_seg_ts[:,j+c*n_timestamps]) #norm

        if seg_ts_array is None :
            seg_ts_array = np.stack(lst, axis=1)
        else :
            seg_ts_array = np.hstack((seg_ts_array,np.stack(lst, axis=1)))
    print (seg_ts_array.shape)
    np.save(os.path.join(outPath,"norm_"+os.path.basename(seg_ts_file)),seg_ts_array)

    gt_ts_array = None
    for j in range(n_timestamps): # Time stamps
        lst = []
        for c in range(n_bands) : # bands + indices number
            lst.append(norm_gt_ts[:,j+c*n_timestamps]) #norm

        if gt_ts_array is None :
            gt_ts_array = np.stack(lst, axis=1)
        else :
            gt_ts_array = np.hstack((gt_ts_array,np.stack(lst, axis=1)))
    print (gt_ts_array.shape)
    np.save(os.path.join(outPath,"norm_"+os.path.basename(gt_ts_file)),gt_ts_array)


if __name__ == "__main__":

    seg_ts_file = "/media/je/SATA_1/lab4/data/senegal/mor_allstats.npy"
    gt_ts_file = "/media/je/SATA_1/lab4/data/senegal/opt_data.npy"

    normalize_time_series(seg_ts_file, gt_ts_file, n_bands=12)

    
import sys
import os
import tensorflow as tf
print (tf.__version__)
import numpy as np
from sklearn.preprocessing import LabelEncoder

def transform_label(test_label,test_prediction):
    encoder = LabelEncoder()
    encoder.fit(test_label)
    prediction = encoder.inverse_transform(test_prediction)
    print (prediction.shape)
    return prediction

def format_to_cnn1d (X, n_bands):
    new_X = np.reshape(X,(X.shape[0],n_timestamps,-1))
    print (new_X.shape)
    return new_X

def get_batch(array, i, batch_size):
    start_id = i*batch_size
    end_id = min((i+1) * batch_size, array.shape[0])
    batch = array[start_id:end_id]
    return batch

def restore (test_X, ckpt_path, batch_size, level) :
    tf.reset_default_graph()
    with tf.Session() as session :
        # Restore variables from disk.
        model_saver = tf.train.import_meta_graph(ckpt_path+".meta")
        model_saver.restore(session, ckpt_path)
        
        graph = tf.get_default_graph()

        X = graph.get_tensor_by_name("X:0")
        dropOut = graph.get_tensor_by_name("drop_rate:0")
        prediction = graph.get_tensor_by_name("prediction/pred")
        
        print ("Model restored. "+ckpt_path)

        n_batch = int(test_X.shape[0] / batch_size)
        if test_X.shape[0] % batch_size != 0:
            n_batch+=1
        print ("n_batch: %d" %n_batch)

        total_pred = None
            
        for batch in range(n_batch):
            batch_X = get_batch(test_X,batch,batch_size)

            batch_pred = session.run(prediction,feed_dict={X:batch_X,
                                                           dropOut:0.})
            del batch_X
            
            if total_pred is None :
                total_pred = batch_pred
            else : 
                total_pred = np.hstack((total_pred,batch_pred))

    return total_pred

if __name__ == '__main__':

    # Reading data
    test_ts = np.load(sys.argv[1])
    print ("test_ts:", test_ts.shape)

    test_label = np.load(sys.argv[2])

    split_numb = int(sys.argv[3])
    model_directory = sys.argv[4]
    n_timestamps = int(sys.argv[5])
    level = int(sys.argv[6])

    test_label = test_label[:,level]
    test_label = test_label.astype('int64')
    print ("test_label:", test_label.shape)

    sys.stdout.flush

    # Format data and label
    test_X = format_to_cnn1d(test_ts, n_timestamps)

    # Run Model
    ckpt_path = os.path.join(model_directory,"model_%s"%str(split_numb))
    results_path = os.path.join(model_directory,"results")
    if not os.path.exists(results_path):
        os.makedirs(results_path)

    batch_size = 128

    test_prediction = restore(test_X, ckpt_path, batch_size, level-1)
    prediction = transform_label(test_label,test_prediction)
    np.save(os.path.join(results_path,"results_"+str(split_numb)+".npy"), prediction)

    print ("TEST RESULTS SAVED")